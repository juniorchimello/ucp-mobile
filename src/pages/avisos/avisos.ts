import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {HttpClient} from "@angular/common/http";
import { Storage } from '@ionic/storage';

/**
 * Generated class for the AvisosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-avisos',
  templateUrl: 'avisos.html',
})
export class AvisosPage {

  public avisos:any;
  public currentSelected:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public httpClient: HttpClient, private storage: Storage) {
  }

  ionViewDidLoad() {
    let self = this;

    self.storage.get('usuario').then( function(data) {
      if ( !data ) {
        self.navCtrl.setRoot('LoginPage');
      }

      let url = 'http://apiapp.hostbrs.com.br/alunos/'+ data.id +'/avisos';
      self.httpClient.get(url)
        .subscribe( res => {
          self.avisos = res;
        })
    });
  }

  seleciona( idx ):void{
    this.currentSelected = idx;
  }

}
