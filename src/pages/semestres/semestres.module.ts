import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SemestresPage } from './semestres';

@NgModule({
  declarations: [
    SemestresPage,
  ],
  imports: [
    IonicPageModule.forChild(SemestresPage),
  ],
})
export class SemestresPageModule {}
