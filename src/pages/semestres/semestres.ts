import {Component} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {HttpClient} from "@angular/common/http";
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the SemestresPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-semestres',
  templateUrl: 'semestres.html',
})
export class SemestresPage {
  public matricula_id: any;
  public semestre_id: any;
  public semestre_nome: any;
  public semestres: any;
  public opcoes:boolean = false;
  public currentSelected:any;


  constructor(public navCtrl: NavController, public navParams: NavParams, public httpClient: HttpClient, private storage: Storage) {
    this.matricula_id = navParams.get('matricula_id');
  }

  ionViewDidLoad() {
    let self = this;
    self.storage.get('usuario').then( function(data) {
      self.httpClient.get('http://apiapp.hostbrs.com.br/alunos/'+data.id+'/matriculas/'+self.matricula_id+'/semestres')
        .subscribe( res => {
          self.semestres = res;
        });
    });
  }

  openOpcoes( semestre_id, semestre_nome, idx ) {
    this.opcoes = true;
    this.currentSelected = idx;
    this.semestre_id = semestre_id;
    this.semestre_nome = semestre_nome;
  }

  openNotas( semestre_id, semestre_nome ):void {
    this.navCtrl.push('NotasPage', {
      matricula:this.matricula_id,
      semestre_id:semestre_id,
      semestre_nome:semestre_nome
    });
  }

  openFrequencia( semestre_id, semestre_nome ):void {
    this.navCtrl.push('FrequenciaPage', {
      matricula_id:this.matricula_id,
      semestre_id:semestre_id,
      semestre_nome:semestre_nome
    });
  }

}
