import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {HttpClient} from "@angular/common/http";
import { Storage } from '@ionic/storage';
import { ScreenOrientation } from '@ionic-native/screen-orientation';


/**
 * Generated class for the NotasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-notas',
  templateUrl: 'notas.html',
})
export class NotasPage {
  public semestre_id:any;
  public semestre_nome:any;
  public matricula:any;
  public notas:any;
  public temNotas:boolean = false;
  public rows:any;
  public cols:any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public httpClient: HttpClient, 
    private storage: Storage,
    private screenOrientation: ScreenOrientation
    ) {
    this.semestre_id  = navParams.get('semestre_id');
    this.semestre_nome = navParams.get('semestre_nome');
    this.matricula = navParams.get('matricula');
    this.cols = [
      {prop: "nome_disciplina", name:"Nome", width: "250" },
      {prop: "parcial_1", name:"P1"},
      {prop: "parcial_2", name:"P2"},
      {prop: "trabalhos", name:"Trabalhos"},
      {prop: "extras", name:"Extras"},
      {prop: "total_parcial", name:"Total Parcial"},
      {prop: "prova_final", name:"Prova Final"},
      {prop: "total_geral", name:"Total Geral"},
    ];

    // get current
    //console.log(this.screenOrientation.type); // logs the current orientation, example: 'landscape'

    // set to landscape
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);

    // allow user rotate
    this.screenOrientation.unlock();

    // detect orientation changes
    this.screenOrientation.onChange().subscribe(
      () => {
          console.log("Orientation Changed");
      }
    );
  }

  ionViewDidLoad() {
    let self = this;
    self.storage.get('usuario').then( function(data) {
      self.httpClient.get('http://apiapp.hostbrs.com.br/alunos/' + data.id + '/matriculas/' + self.matricula + '/semestres/' + self.semestre_id + '/notas')
        .subscribe(res => {
          self.temNotas = true;
          for (let index = 0; index < Object.keys(res).length; index++) {
            res[index].total_geral = parseInt(res[index].parcial_1) + parseInt(res[index].parcial_2) + parseInt(res[index].trabalhos) + parseInt(res[index].prova_final);
            res[index].total_parcial = parseInt(res[index].parcial_1) + parseInt(res[index].parcial_2) + parseInt(res[index].trabalhos);       
          }
          self.rows = res;
        });
    });

  }

  ionViewWillLeave() {
    // get current
    console.log(this.screenOrientation.type); // logs the current orientation, example: 'landscape'
  
    // set to landscape
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
  
    // allow user rotate
    this.screenOrientation.unlock();
  
    // detect orientation changes
    this.screenOrientation.onChange().subscribe(
      () => {
            console.log("Orientation Changed");
      }
    );
  }

}
