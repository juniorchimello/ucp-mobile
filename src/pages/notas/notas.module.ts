import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NotasPage } from './notas';
import {NgxDatatableModule} from "@swimlane/ngx-datatable";

@NgModule({
  declarations: [
    NotasPage,
  ],
  imports: [
    NgxDatatableModule,
    IonicPageModule.forChild(NotasPage),
  ],
})
export class NotasPageModule {}
