import {Component} from '@angular/core';
import {IonicPage, MenuController, NavController, NavParams} from 'ionic-angular';
import {HttpClient} from "@angular/common/http";
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-matriculas',
  templateUrl: 'matriculas.html',
})
export class MatriculasPage {

  id:any;
  matriculas:Object;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public httpClient: HttpClient,
    public menuCtrl: MenuController,
    private storage: Storage
  ) {}

  ionViewDidLoad() {
    let self = this;

    self.storage.get('usuario').then( function(data) {
      if ( !data ) {
        self.navCtrl.setRoot('LoginPage');
      }

      let url = 'http://apiapp.hostbrs.com.br/alunos/'+ data.id +'/matriculas';
      self.httpClient.get(url)
        .subscribe( res => {
          self.matriculas = res;
        })
    });
  }

  openSemestres( matricula_id ):void {
    this.navCtrl.push('SemestresPage', {matricula_id:matricula_id});
  }

}
