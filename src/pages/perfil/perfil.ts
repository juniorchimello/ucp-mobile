import { Component } from '@angular/core';
import {RequestOptions, Request, RequestMethod} from '@angular/http';
import {IonicPage, MenuController, NavController, NavParams,LoadingController, ToastController} from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { HttpClient } from '@angular/common/http';

/**
 * Generated class for the PerfilPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

export interface DadosRetorno {
  status: string;
  message: string;
  url: string;
}

@IonicPage()
@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
})
export class PerfilPage {

  public nome:string;
  public sobrenome:string;
  public idade: string;
  public data_nascimento: string;
  public curso: string;
  public semestre: string;
  public matrícula_atual: string;
  public foto:string;
  public filial:string;
  public rg:string;

  public imageURI:any;
  public imageFileName:any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    private transfer: FileTransfer,
    private camera: Camera,
    private storage: Storage,
    private httpClient: HttpClient
  ) {
    this.menuCtrl.enable(true, 'ucpMenu');

    let self = this;
    self.storage.get('usuario').then( function(data) {
      console.log(data);
      self.nome = data.nome;
      self.sobrenome = data.sobrenome;
      self.data_nascimento = data.data_nascimento;
      self.foto = data.imagem;
      self.filial = (data.filial == '2') ? 'Pedro Juan Caballero': (data.filial == '3') ? 'Ciudad del Este Sede III' : 'Ciudad del Este Sede II';
      self.rg = data.rg;
    });
  }

  getImage( type ) {
    const options: CameraOptions = {
      quality: 40,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType:this.camera.MediaType.PICTURE,
      sourceType: type == "picture" ? this.camera.PictureSourceType.CAMERA : this.camera.PictureSourceType.SAVEDPHOTOALBUM,
      correctOrientation: true
    }
  
    this.camera.getPicture(options).then((imageData) => {
     this.imageURI = 'data:image/jpeg;base64,'+imageData;
    }, (err) => {
      console.log(err);
      this.presentToast(err);
    });
  }

  uploadFile() {
    let loader = this.loadingCtrl.create({
      content: "Uploading..."
    });
    loader.present();

    let self = this;
    self.storage.get('usuario').then( function(usuario) { 
      self.httpClient.post<DadosRetorno>('https://apiapp.hostbrs.com.br/alunos/'+usuario.id+'/upload',{imagem: self.imageURI}).subscribe((response) => {
        console.log(JSON.stringify( response ));
        self.imageFileName = true;
        loader.dismiss();
        self.presentToast("Imagem salva com sucesso.");
        // atalizando foto
        self.foto = 'fotos_alunos/' + response.url;
        self.storage.set('usuario.foto', self.foto );
        location.reload();
      }, (err) => {
        console.log(JSON.stringify(err));
        loader.dismiss();
        self.presentToast(err);
      });
    });
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

}
