import { Component } from '@angular/core';
import {IonicPage, MenuController, NavController, NavParams} from 'ionic-angular';

/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  public currentSelected:any;
  pages: Array<{title: string, icon:string,  component: string}>;

  constructor(public navCtrl: NavController, public navParams: NavParams, public menuCtrl: MenuController) {
    this.menuCtrl.enable(true, 'ucpMenu');

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Avisos', icon:'text' ,component: 'AvisosPage' },
      { title: 'Matriculas', icon:'copy',  component: 'MatriculasPage' },
      { title: 'Financeiro', icon: 'card', component: 'FaturasPage' },
    ];
  }

  openPage(page, idx) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.currentSelected = idx;
    this.navCtrl.push(page.component);
  }

}
