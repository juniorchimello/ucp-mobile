import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {HttpClient} from "@angular/common/http";
import { Storage } from '@ionic/storage';


/**
 * Generated class for the FrequenciaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-frequencia',
  templateUrl: 'frequencia.html',
})
export class FrequenciaPage {

  public matricula_id:any;
  public semestre_id:any;
  public disciplinas: any;

  public semestre_nome:string;
  public currentSelected:any;

  public showGrafico:boolean;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public httpClient: HttpClient,
    private storage: Storage
  ) {
    this.matricula_id = navParams.get('matricula_id');
    this.semestre_id = navParams.get('semestre_id');
    this.semestre_nome = navParams.get('semestre_nome');
    this.showGrafico = false;
  }

  ionViewDidLoad() {
    let self = this;
    self.storage.get('usuario').then( function(data) {
      self.httpClient.get('http://apiapp.hostbrs.com.br/alunos/'+data.id+'/matriculas/' + self.matricula_id + '/semestres/' + self.semestre_id + '/disciplinas')
        .subscribe(res => {
          self.disciplinas = res;
        });
    });
  }

  openGraficoFrequencia(disciplina_id, disciplina_nome, idx):void {
    this.currentSelected = idx;
    this.navCtrl.push('GraficoFrequenciaPage',{
      matricula_id: this.matricula_id,
      semestre_id: this.semestre_id,
      disciplina_id: disciplina_id,
      disciplina_nome: disciplina_nome
    });
  }
}
