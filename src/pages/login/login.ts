import { Component } from '@angular/core';
import {
  IonicPage,
  LoadingController,
  NavController,
  NavParams,
  AlertController, MenuController
} from 'ionic-angular';
import {HttpClient} from "@angular/common/http";
import { Storage } from '@ionic/storage';


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  formData = {
    filial: null,
    rg: null,
    senha: null
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public menuCtrl: MenuController,
    public alertCtrl: AlertController,
    public httpClient: HttpClient,
    private storage: Storage
  ) {
    this.menuCtrl.enable(false, 'ucpMenu');
  }

  ionViewDidLoad() {
    let self = this;
    this.storage.get('usuario').then(function( data ) {
      if ( data ) {
        self.navCtrl.setRoot('HomePage');
      }
    });
  }

  /**
   * Validando credenciais informadas
   */
  login() {

    let loading = this.loadingCtrl.create({
      content: 'Validando credenciais...'
    });

    let rg = this.formData.rg;
    let senha = this.formData.senha;
    let filial = this.formData.filial;

    loading.present();

    this.httpClient.post('http://apiapp.hostbrs.com.br/login',{
      rg: rg,
      senha: senha,
      filial: filial
    })
      .subscribe(data => {
        this.storage.set('usuario', data);
        let credenciais = {rg: rg, senha: senha, filial: filial};
        this.storage.set('credenciais', credenciais);
        this.navCtrl.setRoot('PerfilPage');
        loading.dismiss();
      }, (err) => {
        loading.dismiss();
        /** TODO: exibir um alerta de credenciais inválidas! */
      });
  }
}
