import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AvisosDetalhePage } from './avisos-detalhe';

@NgModule({
  declarations: [
    AvisosDetalhePage,
  ],
  imports: [
    IonicPageModule.forChild(AvisosDetalhePage),
  ],
})
export class AvisosDetalhePageModule {}
