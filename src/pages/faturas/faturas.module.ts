import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FaturasPage } from './faturas';
import {NgxDatatableModule} from "@swimlane/ngx-datatable";

@NgModule({
  declarations: [
    FaturasPage,
  ],
  imports: [
    IonicPageModule.forChild(FaturasPage),
    NgxDatatableModule
  ],
})
export class FaturasPageModule {}
