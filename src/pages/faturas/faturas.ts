import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {HttpClient} from "@angular/common/http";
import { Storage } from '@ionic/storage';


/**
 * Generated class for the FaturasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-faturas',
  templateUrl: 'faturas.html',
})
export class FaturasPage {

  public matriculas:any;
  public semestres:any;

  public matricula_id:any;
  public semestre_id:any;

  public faturas:any;
  public temFatura:boolean = false;
  public rows:any;
  public cols:any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public httpClient: HttpClient,
    private storage: Storage)
  {
    this.cols = [
      {name: "Departamento", prop:"nome_departamento", width:250},
      {name: "Centro de Custo", prop:"nome_centro_custo", width:250},
      {name: "Nº Documento", prop:"numero"},
      {name: "Valor", prop:"valor"},
      {name: "Desconto", prop:"desconto"},
      {name: "Valor Pago", prop:"valor_pago"},
      {name: "Vencimento", prop:"data_vencimento", width:240},
      {name: "Pagamento", prop:"data_pagamento", width:240},
      {name: "Status", prop:"status"},
    ]
  }

  ionViewDidLoad() {
    let self = this;
    self.storage.get('usuario').then( function(data) {
      if ( !data ) {
        self.navCtrl.setRoot('LoginPage');
      }

      self.httpClient.get('http://apiapp.hostbrs.com.br/alunos/'+data.id+'/matriculas')
        .subscribe(res => {
          self.matriculas = res;
        });
    });
  }

  getSemestres( matricula_id ):void {
    let self = this;
    self.storage.get('usuario').then( function(data) {
      self.httpClient.get('http://apiapp.hostbrs.com.br/alunos/'+data.id+'/matriculas/' + matricula_id + '/semestres')
        .subscribe(res => {
          self.semestres = res;
        });
    });
  }

  openFaturasDetalhes():void {
    let self = this;
    self.storage.get('usuario').then( function(data) {
      self.httpClient.get('http://apiapp.hostbrs.com.br/alunos/' + data.id + '/matriculas/' + self.matricula_id + '/semestres/' + self.semestre_id + '/faturas')
        .subscribe(res => {
          self.rows = res;
          self.temFatura = true;
        });
    });
  }

}
