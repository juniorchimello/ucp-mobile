import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GraficoFrequenciaPage } from './grafico-frequencia';

@NgModule({
  declarations: [
    GraficoFrequenciaPage,
  ],
  imports: [
    IonicPageModule.forChild(GraficoFrequenciaPage),
  ],
})
export class GraficoFrequenciaPageModule {}
