import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {HttpClient} from "@angular/common/http";
import { Storage } from '@ionic/storage';
import * as CanvasJS from './canvasjs.min';


/**
 * Generated class for the GraficoFrequenciaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-grafico-frequencia',
  templateUrl: 'grafico-frequencia.html',
})
export class GraficoFrequenciaPage {

  public matricula_id:any;
  public semestre_id:any;
  public disciplina_id:any;
  public disciplina_nome:string;

  public chartData:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public httpClient: HttpClient, private storage: Storage) {
    this.matricula_id = navParams.get('matricula_id');
    this.semestre_id = navParams.get('semestre_id');
    this.disciplina_id = navParams.get('disciplina_id');
    this.disciplina_nome = navParams.get('disciplina_nome');
  }

  ionViewDidLoad() {
    let self = this;

    self.storage.get('usuario').then( function(data) {
      self.httpClient.get('http://apiapp.hostbrs.com.br/alunos/'+data.id+'/matriculas/' + self.matricula_id + '/semestres/' + self.semestre_id + '/disciplinas/'+self.disciplina_id)
        .subscribe(res => {
          let dados:any;
          dados = res;

          let faltas = dados.aulas - dados.presenca;
          let presencas = dados.presenca;

          let chart = new CanvasJS.Chart("chartContainer", {
            theme: "light2",
            animationEnabled: true,
            exportEnabled: true,
            title:{
              text: "Frequência "+ self.disciplina_nome
            },
            data: [{
              type: "pie",
              showInLegend: true,
              toolTipContent: "<b>{name}</b>: {y} (#percent%)",
              indexLabel: "{name} - #percent%",
              dataPoints: [
                {name:"Faltas", y: faltas },
                {name:"Presente", y: presencas},
              ]
            }]
          });

          chart.render();
        });
    });
  }

}
